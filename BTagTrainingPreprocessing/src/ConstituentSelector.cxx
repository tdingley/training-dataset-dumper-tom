#include "ConstituentSelector.hh"
#include "xAODPFlow/TrackCaloCluster.h"

ConstituentSelector::ConstituentSelector() {}

ConstituentSelector::TCCs ConstituentSelector::get_constituents(const xAOD::Jet& jet) const
{
 
  std::vector<const xAOD::TrackCaloCluster*> tccs;
  for ( size_t c = 0; c < jet.numConstituents(); ++c ) {
    auto link_tcc = jet.constituentLinks().at(c);
    const xAOD::TrackCaloCluster* tcc = dynamic_cast<const xAOD::TrackCaloCluster*>(*link_tcc);
    tccs.push_back(tcc);
  }
  return tccs;
}
